#include "setup.h"

#include "mb_axis3d.h" // ��� � ������������ MbAxis3D
#include "mb_placement3d.h" // ������� ��������� MbPlacement3D
#include "point_frame.h" // �������� ������ MbPointFrame
#include "cur_line_segment3d.h" // ������� MbLineSegment3D
#include "surf_plane.h" // ��������� MbPlane
#include "surf_sphere_surface.h" // ����������� ����������� MbSphereSurface
#include "surf_cylinder_surface.h" // �������������� ����������� MbCylinderSurface
#include "surf_cone_surface.h" // ���������� ����������� MbConeSurface
#include "action_surface.h" // ������� ���������� ������������

using namespace c3d;

bool run()
{
    // ����������� ��� �������������� �������� ����� �� �������� � �������
    const double DEG_TO_RAD = M_PI/180.0;
    // ���������� ���������, ����������� � ������������ ���������� XZ.
    // ������������ ����� ������������ ��������� �� ���� ������.
    // � ������ ������ ����� ���� �������� ������������ �����, ������� � ��������� XZ.
    // �������� ��������� ������ �� ������� ��������� ��� ����������� �
    // �������� ����������.
    MbCartPoint3D planePnt1( 0, 0, 0 );
    MbCartPoint3D planePnt2( 50, 0, 0 );
    MbCartPoint3D planePnt3( 0, 0, 50 );
    MbPlane* pPlane = new MbPlane( planePnt1, planePnt2, planePnt3 );
    // ����������� ��������� ����� ������
    if ( pPlane )
        show( Style( 1, LIGHTBLUE ), pPlane );
    // ����� ������������ ���������� ������������ ������������, ���������� ��������� XZ
    // � ������� � �� ������������� ����������������.
    // ���������� ����� ����������� ������ ������ ������������ � ��������� ������ �
    // ������� ����������� �����������
    double radius_Sph1 = 10;
    MbCartPoint3D centerPnt_Sph1( -radius_Sph1*3, radius_Sph1, -radius_Sph1*3 );
    MbSphereSurface* pSphere1 = new MbSphereSurface( centerPnt_Sph1, radius_Sph1 );

    if ( pSphere1 )
        show( Style( 1, LIGHTRED ), pSphere1 );
    // ���������� ����� ������� ������� ���� ��� �������� ������������ �����������
    // ���������� �����-����������:
    // 1) ������ ��������� �� �����������;
    // 2) �����, ������������ ����������� ��� � ��������� �� � ������ �����������;
    // 3) �����, ������������ ����������� ��� Y ��������� ��.
    // ��������� �������� ���, ����� ������ ���� ���� ������������� �� ������ ������� �
    // �� ������ ���������� �� ��� OY ������� ��
    MbSurface* pSphere2 = NULL;
    double radius_Sph2 = 7;
    MbCartPoint3D centerPnt_Sph2( -centerPnt_Sph1.x, radius_Sph2, -centerPnt_Sph1.z );
    MbCartPoint3D pntOX_Sph2 = centerPnt_Sph2 + MbVector3D( radius_Sph2, 0, 0 );
    MbCartPoint3D pntOY_Sph2 = centerPnt_Sph2 + MbVector3D( 0, radius_Sph2, 0 );
    MbResultType resSph2 = ::ElementarySurface( centerPnt_Sph2, pntOX_Sph2,
    pntOY_Sph2, st_SphereSurface, pSphere2 );
    if ( resSph2 == rt_Success )
        show( Style( 1, LIGHTGREEN ), pSphere2 );
    // ���������� ������ �������������� �����������
    // ����� ������������ � ��������� ������ ��������� ��, ������� ��������� � ������.
    // ��� �������� ������������� ����� ��� Z ��������� ��
    double radius_Cyl1 = 4;
    double height_Cyl1 = 15;
    MbPlacement3D plCyl1;
    // ��������� �� ��������: ������� �� ��������� �� �������� ������ � ��������������
    // ���, ����� ��� Z ��������� �� ���� ����������� ��� OY ������� ��
    plCyl1.Rotate(MbAxis3D(MbVector3D(1,0,0)), -90*DEG_TO_RAD );
    plCyl1.Move( MbVector3D( radius_Cyl1*5, 0, -radius_Cyl1*5 ) );
    MbCylinderSurface* pCyl1 = new MbCylinderSurface( plCyl1, radius_Cyl1, height_Cyl1 );
    if ( pCyl1 )
        show( Style( 1, LIGHTRED ), pCyl1 );
    // ���������� ������ �������������� �����������
    // ����� ������� ���� ��� �������� ������������ �����������
    MbSurface* pCyl2 = NULL;
    double radius_Cyl2 = 2.5;
    double height_Cyl2 = 10;
    // ����� ������� ��������� �������� (������ ��������� ��������� ��) ��������
    // ���������� ������ ��������� ������� �������� ������������ ��� OY ������� ��
    MbCartPoint3D centerPnt_Cyl2 = plCyl1.GetOrigin();
    centerPnt_Cyl2.x = -centerPnt_Cyl2.x;
    centerPnt_Cyl2.z = -centerPnt_Cyl2.z;
    // �����, �������� ��� X ��������� �� � ������������ ������ ��������
    // � ������ ������ ��� X ��������� �� ����������� ��� Y ������� ��
    MbCartPoint3D pntOX_Cyl2 = centerPnt_Cyl2 + MbVector3D( 0, height_Cyl2, 0 );
    // �����, �������� ��� Y ��������� �� � ������������ ������ ���������
    // � ������ ������ ��� Y ��������� �� ����������� ��� Z ������� ��
    MbCartPoint3D pntOY_Cyl2 = centerPnt_Cyl2 + MbVector3D( 0, 0, radius_Cyl2 );
    MbResultType resCyl2 = ::ElementarySurface( centerPnt_Cyl2, pntOX_Cyl2,
    pntOY_Cyl2, st_CylinderSurface, pCyl2 );
    if ( resCyl2 == rt_Success )
        show( Style( 1, LIGHTGREEN ), pCyl2 );
    // ���������� ������ ���������� �����������
    // ��� ����������� �������� ��������� XZ ������� �� � ����� �����
    // ���������� ����������� ���������� ����������� �� ���� ������.
    double height_Cone1 = 15;
    double radius_Cone1 = 7.5;
    // ������� ���������� �����������
    MbCartPoint3D pntCone1_origin( 10, 0, 0 );
    // ����� ���������
    MbCartPoint3D pntCone1_base = pntCone1_origin + MbVector3D(0, height_Cone1, 0 );
    // ����� �� ������� ����������� � ��������� ���������
    MbCartPoint3D pntCone1_surf = pntCone1_base + MbVector3D(radius_Cone1, 0, 0 );
    MbConeSurface* pCone1 = new MbConeSurface( pntCone1_origin, pntCone1_base,
    pntCone1_surf );
    if (pCone1)
        show( Style( 1, LIGHTRED ), pCone1 );
    // ���������� ������ ���������� �����������
    // ��������� ���� ����������� ����� �� ��������� XZ ������� ��
    double height_Cone2 = 8;
    double radius_Cone2 = 4;
    // ������� ���������� �����������
    MbCartPoint3D pntCone2_origin( -pntCone1_origin.x, height_Cone2, 0 );
    // ����� ���������
    MbCartPoint3D pntCone2_base = pntCone2_origin + MbVector3D(0, -height_Cone2, 0 );
    // ����� �� ������� ����������� � ��������� ���������
    MbCartPoint3D pntCone2_surf = pntCone2_base + MbVector3D(radius_Cone2, 0, 0 );
    MbSurface* pCone2 = NULL;
    MbResultType resCone2 = ::ElementarySurface( pntCone2_origin, pntCone2_base,
    pntCone2_surf, st_ConeSurface, pCone2 );
    if ( resCone2 == rt_Success )
        show( Style( 1, LIGHTGREEN ), pCone2 );
    // ���������� �������� ������ �� ����������� ��������� �������.
    // ��� ���������� ��������� 0 ������ ����� ������ ������� �����������.
    ::DeleteItem( pPlane );
    ::DeleteItem( pSphere1 );
    ::DeleteItem( pSphere2 );
    ::DeleteItem( pCyl1 );
    ::DeleteItem( pCyl2 );
    ::DeleteItem( pCone1 );
    ::DeleteItem( pCone2 );

    return true;
}