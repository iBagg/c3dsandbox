#include "setup.h"

#include "mb_cart_point.h"
#include "mb_placement3d.h"
#include "cur_line_segment.h"

using namespace c3d;

bool run()
{
    // ��������� �� (����������� ���, ����� �� ��������� XY ��������� �
    // ���������� XZ ������� ��)
    MbPlacement3D pl;
    pl.SetAxisY( MbVector3D(0, 0, 1) );
    // ������� �������
    MbCartPoint p1(5, 5), p2(15, 7);
    // ������������ �������� �������-�������
    MbLineSegment* pSeg = new MbLineSegment( p1, p2 );
    // ���������� � ����������� � �������� ����������
    if ( pSeg )
        show( RGB(150,0,0),pSeg, &pl);
    return true;
}