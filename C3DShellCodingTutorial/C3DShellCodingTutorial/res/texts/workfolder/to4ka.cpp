#include "setup.h"

#include "mb_cart_point.h"
#include "point_frame.h"

using namespace c3d;

bool run()
{
    // �������� ���� ���������� ����� �� ���� X, Y � Z
    MbCartPoint3D pntX( 1, 0, 0 ), pntY( 0, 1, 0 ), pntZ( 0, 0, 1 );
    // ���������� ����� � ������ c ����������� �� ������ ����������� ���������� �������
    // ������ ����� ����������� � ���� "������������" ��������� �������, ����������
    // �� ������������ �����
    show(Style(5, RGB(255,0,0)), new MbPointFrame(pntX));
    show(Style(5, RGB(0,255,0)), new MbPointFrame(pntY));
    show(Style(5, RGB(0,0,255)), new MbPointFrame(pntZ));
    return true;
}