<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CompilerSearch</name>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="41"/>
        <source>C3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="107"/>
        <source>The first launch of the program</source>
        <translation>Первый запуск программы</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="198"/>
        <source>For the full functioning of the program, as well as the ability to test your code in the sandbox, you need a compiler from Microsoft.</source>
        <translation>Для полноценного функционирования программы, а так же возможности тестировать свой код в песочнице требуется наличие компилятора от компании Microsoft.</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="214"/>
        <source>Take the opportunity to choose the default path, select the compiler from the list, or show the path manually by clicking on the &quot;Browse&quot; button.</source>
        <translation>Воспользуйтесь возможностью выбрать путь по умолчанию, выбрать компилятор из списка, либо укажите путь в ручную, нажав на кнопку &quot;Обзор&quot;.</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="309"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="326"/>
        <source>Skip</source>
        <translation>Пропустить</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="360"/>
        <source>Installation directory</source>
        <translation>Каталог установки</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="492"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">Обзор...</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="603"/>
        <source>Specify the path manually</source>
        <translation>Указать путь вручную</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="627"/>
        <source>Select from the list</source>
        <translation>Выбрать из списка:</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/compilersearch.ui" line="576"/>
        <source>Default (VS2019 Сommunity)</source>
        <oldsource>По умолчанию (VS2019 Community)</oldsource>
        <translation>По умолчанию (VS2019 Community)</translation>
    </message>
    <message>
        <location filename="../mainwindow/compilersearch.cpp" line="120"/>
        <source>Path to vcvars64.bat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/compilersearch.cpp" line="121"/>
        <source>*.bat</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CppCodeBuilder</name>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="99"/>
        <source>Compilation started.</source>
        <translation>Компиляция началась.</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="103"/>
        <source>Compilation failed.</source>
        <translation>Возникла ошибка компиляции.</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="114"/>
        <source>Unloading failed!</source>
        <translation>Возникла ошибка разгрузки!</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="124"/>
        <source>Rendering...</source>
        <translation>Отрисовка...</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="127"/>
        <source>Render completed.</source>
        <translation>Отрисовка завершена.</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="131"/>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="169"/>
        <source>Rendering failed</source>
        <translation>Возникла ошибка отрисовки</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="150"/>
        <source>Compilation completed.</source>
        <translation>Компиляция завершена.</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="159"/>
        <source>Loading failed!</source>
        <translation>Возникла ошибка загрузки!</translation>
    </message>
    <message>
        <location filename="../cppcodebuilder/cppcodebuilder.cpp" line="190"/>
        <source>Linking failed</source>
        <translation>Возникла ошибка компановки</translation>
    </message>
</context>
<context>
    <name>FindTextWidget</name>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="19"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="51"/>
        <source>Find:</source>
        <translation>Найти:</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="61"/>
        <source>Find previous</source>
        <translation>Найти предыдущее</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="71"/>
        <source>Find next</source>
        <translation>Найти далее</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="85"/>
        <source>Whole words</source>
        <translation>Целые слова</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="92"/>
        <source>Case Sensitive</source>
        <translation>Учитывать регистр</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="99"/>
        <source>Wrap around</source>
        <translation>Зациклить поиск</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="106"/>
        <source>Regular expression</source>
        <translation>Регулярное выражение</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="139"/>
        <source>Replace with:</source>
        <translation>Заменить на:</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="156"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="166"/>
        <source>Replace and find</source>
        <translation>Заменить и найти</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="176"/>
        <source>Replace All</source>
        <translation>Заменить все</translation>
    </message>
    <message>
        <location filename="../textedit/ui/findtextwidget.ui" line="212"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../textedit/findtextwidget.cpp" line="68"/>
        <source>The following text wasn&apos;t found.</source>
        <translation>Текст не найден.</translation>
    </message>
    <message>
        <location filename="../textedit/findtextwidget.cpp" line="75"/>
        <source>Search ended.</source>
        <translation>Поиск закончен.</translation>
    </message>
    <message>
        <location filename="../textedit/findtextwidget.cpp" line="82"/>
        <source>Replace all: %1 occurrences replaced.</source>
        <translation>Заменить все: %1 заменено совпадений.</translation>
    </message>
</context>
<context>
    <name>LexerCxx</name>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="366"/>
        <source>Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="369"/>
        <source>Inactive default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="372"/>
        <source>C comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="375"/>
        <source>Inactive C comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="378"/>
        <source>C++ comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="381"/>
        <source>Inactive C++ comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="384"/>
        <source>JavaDoc style C comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="387"/>
        <source>Inactive JavaDoc style C comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="390"/>
        <source>Number</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="393"/>
        <source>Inactive number</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="396"/>
        <source>Keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="399"/>
        <source>Inactive keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="402"/>
        <source>Double-quoted string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="405"/>
        <source>Inactive double-quoted string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="408"/>
        <source>Single-quoted string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="411"/>
        <source>Inactive single-quoted string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="414"/>
        <source>IDL UUID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="417"/>
        <source>Inactive IDL UUID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="420"/>
        <source>Pre-processor block</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="423"/>
        <source>Inactive pre-processor block</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="426"/>
        <source>Operator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="429"/>
        <source>Inactive operator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="432"/>
        <source>Identifier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="435"/>
        <source>Inactive identifier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="438"/>
        <source>Unclosed string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="441"/>
        <source>Inactive unclosed string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="444"/>
        <source>C# verbatim string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="447"/>
        <source>Inactive C# verbatim string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="450"/>
        <source>JavaScript regular expression</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="453"/>
        <source>Inactive JavaScript regular expression</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="456"/>
        <source>JavaDoc style C++ comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="459"/>
        <source>Inactive JavaDoc style C++ comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="462"/>
        <source>Secondary keywords and identifiers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="465"/>
        <source>Inactive secondary keywords and identifiers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="468"/>
        <source>JavaDoc keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="471"/>
        <source>Inactive JavaDoc keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="474"/>
        <source>JavaDoc keyword error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="477"/>
        <source>Inactive JavaDoc keyword error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="480"/>
        <source>Global classes and typedefs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="483"/>
        <source>Inactive global classes and typedefs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="486"/>
        <source>C++ raw string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="489"/>
        <source>Inactive C++ raw string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="492"/>
        <source>Vala triple-quoted verbatim string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="495"/>
        <source>Inactive Vala triple-quoted verbatim string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="498"/>
        <source>Pike hash-quoted string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="501"/>
        <source>Inactive Pike hash-quoted string</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="504"/>
        <source>Pre-processor C comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="507"/>
        <source>Inactive pre-processor C comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="510"/>
        <source>JavaDoc style pre-processor comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="513"/>
        <source>Inactive JavaDoc style pre-processor comment</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="516"/>
        <source>User-defined literal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="519"/>
        <source>Inactive user-defined literal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="522"/>
        <source>Task marker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="525"/>
        <source>Inactive task marker</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="528"/>
        <source>Escape sequence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/lexercxx.cpp" line="531"/>
        <source>Inactive escape sequence</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow/ui/mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/mainwindow.ui" line="36"/>
        <source>Открыть...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="255"/>
        <source>&amp;About</source>
        <translation>&amp;О приложении</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="257"/>
        <source>Show the application&apos;s About box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="259"/>
        <source>About &amp;Qt</source>
        <translation>О &amp;Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="261"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="264"/>
        <source>&amp;New</source>
        <translation>&amp;Новый файл</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="266"/>
        <source>Create a new file</source>
        <translation>Создать новый файл</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="269"/>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть...</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="271"/>
        <source>Open an existing file</source>
        <translation>Открыть существующий файл</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="274"/>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="276"/>
        <source>Save the document to disk</source>
        <translation>Сохранить на диск</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="279"/>
        <source>Save &amp;As...</source>
        <translation>Сохранить &amp;как...</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="281"/>
        <source>Save the document under a new name</source>
        <translation>Сохранить документ под новым именем</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="284"/>
        <source>E&amp;xit</source>
        <translation>В&amp;ыйти</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="286"/>
        <source>Exit the application</source>
        <translation>Закрыть приложение</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="289"/>
        <source>&amp;Find</source>
        <translation>&amp;Найти</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="290"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="292"/>
        <source>Text Edit</source>
        <translation>Редактор кода</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="294"/>
        <source>text edit</source>
        <translation>Редактор кода</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="300"/>
        <source>&amp;Stop</source>
        <translation>&amp;Остановить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="304"/>
        <source>Reload Page</source>
        <translation>Перезагрузить страницу</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="308"/>
        <location filename="../mainwindow/mainwindow.cpp" line="420"/>
        <source>Zoom &amp;In</source>
        <translation>Приблизить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="312"/>
        <location filename="../mainwindow/mainwindow.cpp" line="424"/>
        <source>Zoom &amp;Out</source>
        <translation>Отдалить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="316"/>
        <location filename="../mainwindow/mainwindow.cpp" line="428"/>
        <source>Reset &amp;Zoom</source>
        <translation>Отменить &amp;зумирование</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="320"/>
        <source>Switch &amp;Language</source>
        <translation>Сменить &amp;язык</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="322"/>
        <location filename="../mainwindow/mainwindow.cpp" line="615"/>
        <source>switch to english</source>
        <translation>Перевести на английский</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="337"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="339"/>
        <source>Go back in History</source>
        <translation>Назад в истории</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="352"/>
        <source>Forward</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="354"/>
        <source>Go forward in History</source>
        <translation>Вперед в истории</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="359"/>
        <location filename="../mainwindow/mainwindow.cpp" line="361"/>
        <source>Open offline</source>
        <translation>Открыть локальную версию</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="363"/>
        <location filename="../mainwindow/mainwindow.cpp" line="365"/>
        <source>Open online</source>
        <translation>Открыть онлайн версию</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="368"/>
        <source>Open...</source>
        <translation>Открыть...</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="370"/>
        <source>Open model</source>
        <translation>Открыть модель</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="372"/>
        <source>Add...</source>
        <translation>Добавить...</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="374"/>
        <source>Add model</source>
        <translation>Добавить модель</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="376"/>
        <source>Clean</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="378"/>
        <source>Clean scene</source>
        <translation>Очистить сцену</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="380"/>
        <location filename="../mainwindow/mainwindow.cpp" line="381"/>
        <source>Zoom to fit</source>
        <translation>По габаритам</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="383"/>
        <location filename="../mainwindow/mainwindow.cpp" line="384"/>
        <source>Animated</source>
        <translation>Вращение</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="386"/>
        <location filename="../mainwindow/mainwindow.cpp" line="387"/>
        <source>Front</source>
        <translation>Спереди</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="389"/>
        <location filename="../mainwindow/mainwindow.cpp" line="390"/>
        <source>Rear</source>
        <translation>Сзади</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="392"/>
        <location filename="../mainwindow/mainwindow.cpp" line="393"/>
        <source>Down</source>
        <translation>Снизу</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="395"/>
        <location filename="../mainwindow/mainwindow.cpp" line="396"/>
        <source>Left</source>
        <translation>Слева</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="398"/>
        <location filename="../mainwindow/mainwindow.cpp" line="399"/>
        <source>Right</source>
        <translation>Справа</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="401"/>
        <location filename="../mainwindow/mainwindow.cpp" line="402"/>
        <source>Up</source>
        <translation>Сверху</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="404"/>
        <location filename="../mainwindow/mainwindow.cpp" line="405"/>
        <source>Isometry</source>
        <translation>Изометрия</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="407"/>
        <location filename="../mainwindow/mainwindow.cpp" line="408"/>
        <source>Shaded</source>
        <translation>Затененный</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="410"/>
        <location filename="../mainwindow/mainwindow.cpp" line="411"/>
        <source>Hiddenremoved</source>
        <translation>Скрыть</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="413"/>
        <location filename="../mainwindow/mainwindow.cpp" line="414"/>
        <source>Shaded with edges</source>
        <oldsource>ShadedWithEdges</oldsource>
        <translation>Отрисовка с ребрами</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="416"/>
        <location filename="../mainwindow/mainwindow.cpp" line="417"/>
        <source>Enable body selection</source>
        <translation>Включить захват тел</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="437"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="447"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="457"/>
        <source>Navigator</source>
        <translation>Навигатор</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="469"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="478"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="482"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="489"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="499"/>
        <source>Ready</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="609"/>
        <source>switch to russian</source>
        <translation>Перевести на русский</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="677"/>
        <source>Documentation</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1133"/>
        <source>Tutorials Gallery</source>
        <translation>Галерея уроков</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="667"/>
        <source>Docs</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="916"/>
        <source>Vision</source>
        <translation>Сцена</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="908"/>
        <source>Scene</source>
        <translation>Сцена</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="711"/>
        <source>Auto</source>
        <translation>Автоматически</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="938"/>
        <source>Back to gallery</source>
        <translation>Вернуться в галерею</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1080"/>
        <source>Tutorial</source>
        <translation>Урок</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1072"/>
        <source>back to tutorial</source>
        <translation>Вернуться к уроку</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1124"/>
        <source>Gallery</source>
        <translation>Галерея</translation>
    </message>
    <message>
        <source>Enable ZoomToFit</source>
        <translation type="vanished">Включить по габаритам</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1169"/>
        <location filename="../mainwindow/mainwindow.cpp" line="1178"/>
        <source>Console output</source>
        <translation>Вывод консоли</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1214"/>
        <location filename="../mainwindow/mainwindow.cpp" line="1223"/>
        <source>Scene message</source>
        <translation>Сообщения</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1275"/>
        <location filename="../mainwindow/mainwindow.cpp" line="1283"/>
        <source>Manuals</source>
        <translation>Руководства</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1320"/>
        <source>Text Editor</source>
        <translation>Редактор кода</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1361"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1361"/>
        <source>The &lt;b&gt;Application&lt;/b&gt; Todo.</source>
        <translation>&lt;b&gt; Приложение &lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1582"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1583"/>
        <source>Stop loading the current page</source>
        <translation>Остановить загрузку текущей страницы</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1588"/>
        <source>Reload</source>
        <translation>Перезагрузить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="1590"/>
        <source>Reload the current page</source>
        <translation>Перезагрузить текущую страницу</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="47"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="208"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="213"/>
        <source>Clear Editor</source>
        <translation>Очистить файл</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="214"/>
        <source>Close all Editors</source>
        <translation>Закрыть все файлы</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="216"/>
        <source>Create Editor</source>
        <translation>Создать вкладку</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="62"/>
        <source>Display</source>
        <translation>Отобразить</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="738"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="744"/>
        <source>Highlight</source>
        <translation>Курсор</translation>
    </message>
    <message>
        <location filename="../mainwindow/mainwindow.cpp" line="750"/>
        <source>Selection</source>
        <translation>Область</translation>
    </message>
</context>
<context>
    <name>TextEditManager</name>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="90"/>
        <location filename="../textedit/texteditmanager.cpp" line="757"/>
        <source>Application</source>
        <translation>Приложение</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="91"/>
        <source>Cannot read file %1:
%2.</source>
        <translation>Невозможно открыть файл: %1: %2.</translation>
    </message>
    <message>
        <source>Close Editor</source>
        <translation type="vanished">Закрыть файл</translation>
    </message>
    <message>
        <source>Editor %1  contains unsaved changes?
Would you like to close it?</source>
        <translation type="vanished">Файл %1 имеет несохраненные изменения? Хотите ли вы его закрыть?</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="147"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="148"/>
        <source>Editor %1 contains unsaved changes.</source>
        <translation>Файл %1 содержит несохраненные изменения.</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="149"/>
        <source>Would you like to save it?</source>
        <translation>Вы хотите сохранить его?</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="164"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="165"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="166"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="167"/>
        <source>Yes to all</source>
        <translation>Да, для всех</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="168"/>
        <source>No to all</source>
        <translation>Нет, для всех</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="217"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="669"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="758"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Невозможно записать файл %1: %2.</translation>
    </message>
    <message>
        <location filename="../textedit/texteditmanager.cpp" line="779"/>
        <source>Select one or more files to open</source>
        <translation>Выберите один или более файлов для открытия</translation>
    </message>
</context>
<context>
    <name>TextEditWidget</name>
    <message>
        <location filename="../textedit/ui/texteditwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/ui/texteditwidget.ui" line="49"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../textedit/texteditwidget.cpp" line="82"/>
        <source>Line: %1, Col: %2</source>
        <translation>Строка: %1, Символ: %2</translation>
    </message>
</context>
<context>
    <name>TutorialGalleryWidget</name>
    <message>
        <source>Tutorials Gallery</source>
        <translation type="vanished">Галерея уроков</translation>
    </message>
</context>
<context>
    <name>VisionScene</name>
    <message>
        <location filename="../scene/visionscene.cpp" line="469"/>
        <source>Open models</source>
        <translation>Открыть модели</translation>
    </message>
    <message>
        <location filename="../scene/visionscene.cpp" line="600"/>
        <source>Choice 1</source>
        <translation>Выбор 1</translation>
    </message>
    <message>
        <location filename="../scene/visionscene.cpp" line="602"/>
        <source>Choice 2</source>
        <translation>Выбор 2</translation>
    </message>
    <message>
        <location filename="../scene/visionscene.cpp" line="603"/>
        <source>Choice 3</source>
        <translation>Выбор 3</translation>
    </message>
</context>
<context>
    <name>WelcomeWidget</name>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="20"/>
        <source>C3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="54"/>
        <source>Open recent</source>
        <translation>Открыть недавние</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="169"/>
        <source>Create new project</source>
        <translation>Создать новый проект</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="224"/>
        <source>Open a local folder</source>
        <translation>Открыть проводник</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="288"/>
        <source>Open tutorials gallery</source>
        <translation>Открыть галерею уроков</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="343"/>
        <source>Open Documentation</source>
        <translation>Открыть документацию</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="392"/>
        <source>Open Manuals</source>
        <translation>Открыть руководства</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="513"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="452"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="472"/>
        <source>RU</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow/ui/welcomewidget.ui" line="477"/>
        <source>ENG</source>
        <translation></translation>
    </message>
</context>
</TS>
