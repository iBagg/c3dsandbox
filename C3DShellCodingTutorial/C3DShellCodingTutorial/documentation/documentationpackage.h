﻿#pragma once
#include <QObject>
#include <QString>

// Посредник для взаимодействия C++ с js документации
class DocumentationPackage : public QObject
{
    Q_OBJECT

public:
    explicit DocumentationPackage(QObject* parent = nullptr);
    void setText(const QString& source, const QString& hostUrl); // Поиск из C++

signals:
    // Запрос на поиск
    void signalSearchRequest(const QString& className, const QString& classMethod, const QString& hostUrl);
    // Сообщение об обновление страницы
    void signalUpdateCurrentUrl(const QString& url);
    // Сообщение об успешном поиске
    void signalSearchIsSuccess(bool success);

public slots:
    // Обновление страницы из Js
    void slotUpdateCurrentUrl(const QString& url);
    // Сообщение об успешном поиске из JS
    void slotSearchIsSuccess(bool success);

private:
    bool m_success;
};