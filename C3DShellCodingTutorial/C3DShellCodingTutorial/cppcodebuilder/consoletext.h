﻿#pragma once

#include <QTextEdit>
#include <QDebug>

class ConsoleText : public QTextEdit
{
    Q_OBJECT

public:

    ConsoleText(QWidget* parent = nullptr);

    // Цвет обычного текста
    QColor stdColor() const;
    void setStdColor(QColor c);
    // Цвет ошибок
    QColor errColor() const;
    void setErrColor(QColor c);
    // Цвет предупреждений
    QColor warningColor() const;
    void setWarningColor(QColor c);

    QColor completionColor() const;
    void setCompletionColor(QColor c);
    // Идентификаторы цвета
    enum class ResultType {Standart, Error, Warning, Complete};

signals:
    void signalMoveToError(const QString& strLine);

protected:
    QSize sizeHint() const override;
    void mouseDoubleClickEvent(QMouseEvent* e) override;

private:
    QColor m_stdColor, m_errColor, m_warningColor, m_completionColor;
    QString m_currentLine;
    
public slots:
    // Получение сообщения с идентификатором цвтеа
    void addText(const QString& result, ResultType t = ResultType::Standart);
    // Получение сообщения с определенным цветом
    void addText(const QString& result, const QColor& color);
    // Очистка консоли
    void clear();
};
