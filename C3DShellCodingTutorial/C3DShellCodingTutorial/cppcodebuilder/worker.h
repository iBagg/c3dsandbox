﻿#pragma once

#include <QObject>

class CppCodeBuilder;

/*
 * Обработчик кода библиотеки пользователя
 */
class Worker : public QObject {
    Q_OBJECT
public:
    Worker(CppCodeBuilder* builder);

public slots:
    // Метод для запуска выполнения кода
    void process();
signals:
    // Сигнал сообщающий о завершении выполнения кода
    void finished(bool res);
    // Сигнал передающий коды ошибок
    void error(QString err);
private:
    // Указатель на класс загрузивший библиотеку
    CppCodeBuilder* m_pBuildData;
};