﻿#pragma once

#include <functional>

#include <QString>
#include <QVector>
#include <QMap>

class QDomNode;

struct Tutorial
{
    QString category; // Наименование категории
    QString title; // Наименование урока
    QString source; // Содержимое урока
    QString imagePath; // Путь к иконке
    QString tutorialPath; // Корневой путь к уроку
    QString codePath; // Путь к блокам кода
    QMap<QString, QString> codeBlocks; // Блоки кода
};

class TutorialLoader
{
public:
    TutorialLoader(const QString& directoryPath);

    void readTutorialDirectory();
    QMap<QString, QVector<Tutorial>>& folders();
private:
    void traverseNode(const QString& path, std::function<void(const QDomNode&, const QString& path, Tutorial& tutorial)> traverse);
    void traverseDir(const QString& dirName);
    void traverseTutorial(const QDomNode& node, const QString& path, Tutorial& tutorial);

    QMap<QString, QVector<Tutorial>> m_tutorials;
    QString m_directoryPath;
};
