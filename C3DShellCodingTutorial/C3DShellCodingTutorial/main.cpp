﻿#include <QApplication>
#include <QScopedPointer>
#include <QStyleFactory>
#include <QSurfaceFormat>
#include <QDebug>
#include <QScreen>

#include <vsn_application.h>

#include "mainwindow.h"

/*
 * Вход в программу 
 */
int main(int argc, char** argv)
{
    Math::SetMultithreadedMode(mtm_Off);

    QCoreApplication::setApplicationName(QStringLiteral("C3DShellCodingTutorial"));
    QCoreApplication::setOrganizationName(QStringLiteral("Moscow Polytech"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("NoDomain"));

    QApplication::setStyle(QStyleFactory::create("Fusion"));

    Application vapp;
    QApplication app(argc, argv);

    if (!QtVision::ActivateLicense()) return 0;
    
    app.setWindowIcon(QIcon(QStringLiteral(":/res/icons/c3d-labs.jpg")));
    app.setFont(QFont("Segoe UI", 10));

    // Setup OpenGL
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setSamples(4);
    format.setSwapInterval(0);
    QSurfaceFormat::setDefaultFormat(format);

    QScopedPointer<MainWindow> mainWin(new MainWindow);

    mainWin->setWindowTitle(QStringLiteral("C3D Shell Coding Tutorial"));
    mainWin->showPreview();

    return app.exec();
}