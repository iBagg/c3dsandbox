РУКОВОДСТВО ПОЛЬЗОВАТЕЛЯ
======================
-------------------------------------------
### [Форма приветствия](#title_1)
### [Формы программы](#title_2)
### [Построение модели](#title_3)
### [Редактор кода](#title_4)
### [Форма визуализации](#title_5)
### [Загрузка примеров из уроков](#title_6)
### [Добавление уроков и файлов pdf формата в программу](#title_7)
-------------------------------------------

[]Программа разработана для того, чтобы предоставить пользователю удобный инструмент для работы со всеми видами документации и примерами с использованием библиотеки C3D Toolkit. Данная оболочка представляет возможность ускоренного обучения ядра c3d непосредственно через написание кода.  
[]При первом запуске программы, пользователя встречает форма с выбором компилятора. Если компилятор совпадает с компилятором по умолчанию, то нажать кнопку «Продолжить», иначе указать путь к текущему компилятору или указать из предложенных и нажать кнопку «Продолжить» (рисунок 1).  

![](res/compile_form.jpg)  
**Рис. 1.** Форма выбора компилятора.

#### <a name="title_1"> []()Форма приветствия </a>
-----------------------
[]После выбора компилятора, при последующих запусках программы пользователя будет встречать стартовая форма (рисунок 2).  

![](res/start_form.jpg)  
**Рис. 2.** Форма приветствия.

[]При запуске программы пользователю предлагаются 5 действий, и также возможность смены языка:  
1. Кнопка «Создать новый проект» открывает основную форму и создает новый проект.
2. Кнопка «Открыть проводник» позволяет пользователю выбрать существующий файл, после откроется основная форма и в редактор кода будет помещен текст из выбранного файла.
3. Кнопка «Открыть галерею уроков» открывает основную форму и сразу открывает галерею с уроками.
4. Кнопка «Открыть документацию» открывает основную форму и сразу открывает документацию.
5. Кнопка «Открыть руководства» открывает основную форму и сразу открывает руководства.
6. Выбор языка для программы.
7. Открыть форму с выбором компилятора, для того, чтобы задать путь к новому.
8. Окно для выбора предыдущих работ. Если пользователь сохранил проект, то при следующем запуске программы, в этом окне будет возможность сразу открыть этот проект.

#### <a name="title_2"> []()Формы программы </a>
-----------------------
[]Для того, чтобы отрыть нужную форму, требуется нажать на соответствующую кнопку, (ещё раз нажатие на кнопку закрывает соответствующую вкладку). Формы для открытия документации, галереи уроков и формы визуализации можно открыть через меню во вкладке «Вид» или при помощи горячих клавиш: «Ctrl» + «Shift» + «D», «Ctrl» + «Shift» «G», «Ctrl» + «Shift» + «V» соответственно. Расположение кнопок показаны на рисунке 3. Действия кнопок:
1. Кнопка открывает документацию DOxygen ядра c3d.
2. Кнопка открывает галерею с уроками.
3. Кнопка открывает форму для отображения моделей.
4. Кнопка возвращает пользователя на то место в уроке, где пользователь остановился и сменил или закрыл вкладку с уроком. Кнопка становится активной после того, как будет открыт хотя бы 1 урок.
5. Кнопка открывает вкладку с pdf файлами.
6. Кнопка открывает редактор кода.
7. Кнопка открывает консоль.
8. Кнопка открывает форму для сообщений, которые пользователь оставил в редакторе кода при помощи функции message.

![](res/icons_location.jpg)  
**Рис. 3.** Расположение кнопок для открытия форм.

#### <a name="title_3"> []()Построение модели </a>
-----------------------
[]Для построения модели, требуется написать код для построение модели в редакторе кода, после этого нажать на кнопку «Отобразить», если код написан правильно, то в модуле визуализации будет отображена модель (рисунок 4). Если скомпилировать код из редактора кода не удалось, то в консоли будет отображено, где совершена ошибка(рисунок 5). Если ошибка синтаксическая, то при нажатии 2 раза левой кнопкной мыши на сообщение в консоли, курсор переместится на это место в редакторе кода.    

![](res/successful_build.jpg)  
**Рис. 4.** Успешное построение модели.

![](res/bad_build.jpg)  
**Рис. 5.** Сообщение об ошибке.

#### <a name="title_4"> []()Редактор кода </a>
-----------------------
[]В программе реализована возможность работать сразу с несколькими вкладками для того, чтобы создать вкладку требуется нажать на выделенные кнопки (рисунок 6), или в меню во вкладке «Файл» нажать кнопку «Новый файл» (также создать новую вкладку можно при помощи сочетания клавиш «Ctrl+N»).

![](res/add_tab.jpg)  
**Рис. 6.** Создание новой вкладки.

[]Для того чтобы открыть файл (файл или файлы откроются в новых вкладках), требуется нажать на кнопку, показанную на рисунке 7 или меню нажать на кнопку «Открыть…» (сочетание клавиш «Ctrl» + O).  

![](res/open.jpg)  
**Рис. 7.** Открыть файл.

[]Для сохранения файла требуется нажать на кнопку, показанную на рисунке 8 или в меню нажать на одну из кнопок «Сохранить» («Ctrl» + «S») или «Сохранить как…» («Ctrl» + «Shift» + «N»)  

![](res/save.jpg)  
**Рис. 8.** Сохранить файл.

[]Также есть ещё несколько дополнительных возможностей взаимодействия с редактором кода (рисунок 9).  

![](res/toolbar_texteditor.jpg)  
**Рис. 9.** Дополнительные функции.

1. Первая кнопка является выпадающим список, где пользователь может выбрать 2 действия: удалить содержимое в текущей вкладке в редакторе кода и закрыть все вкладки.
2. Вторая кнопка тоже является выпадающим списком. В этом выпадающем списке отображаются названия вкладок в редакторе кода, кликнув по которой, откроется вкладка.
3. Третья кнопка «вытаскивает редактор кода», то есть редактор кода становится как отдельное окно.
4. Четвертая кнопка сворачивает форму с редактором кода.

Форму поиска и замены текста в реакторе кода можно открыть через меню, во вкладке «Правка» или при помощи сочетания клавиш «Ctrl» + «F». Форма поиска и замены слов показана на рисунке 10.

![](res/find_form.jpg)  
**Рис. 10.** Форма поиска и замены слов.

[]Если в редакторе кода использовать функцию message. То в форме «Сообщения» будет выведено сообщение (рисунок 11) после нажатия кнопки «Отобразить». Также есть функция messageBox (рисунок 12). Сообщения выводятся только при успешной компиляции.  

![](res/example_message.jpg)  
**Рис. 11.** Использование функции message.

![](res/messageBox.jpg)  
**Рис. 12.** Использование функции messageBox.

[]Текст из редактора кода, можно найти в документации, для этого требуется:  
1. Выделить слово или поставить курсор на слово, которое требуется найти.
2. Нажать клавишу «F1».

#### <a name="title_5"> []()Форма визуализации </a>
-----------------------
[]Форма визуализации предназначена для отображения модели на основе кода написанного в редакторе кода. У формы визуализации есть панель инструментов (рисунок 13). Функции на панели инструментов:  
1. Кнопка (открыть) загружает модель (расширение c3d) в форму визуализации.
2. Кнопка (добавить) загружает к текущей модели ещё модель.
3. Кнопка (очистить) очищает сцену от деталей.
4. Кнопка (по габаритам) выравнивает отображение по габаритам модели.
5. При установленном флаге «Автоматически», следующие модели будут сразу выравниваться по габаритам модели.
6. Кнопка (вращение) запускает анимацию вращения вокруг модели.
7. Задать цвет для модели.
8. Задать цвет для наведения курсора на модель.
9. Задать цвет для области модели.
10. Кнопка включает захват тел.
11. Каждая из этих кнопок разворачивают камеру сцены в соответствии с изображением каждой кнопки.
12. Переключение режимов отображения модели.

![](res/visualization_form.jpg)  
**Рис. 13.** Форма визуализаци.

#### <a name="title_6"> []()Загрузка примеров из уроков </a>
-----------------------
[]Примеры из уроков можно загрузить в редактор кода и посмотреть результат в форме визуализации. Для этого требуется:  
1. Открыть галерею с уроками.
2. Выбрать урок.
3. Найти пример (листинг кода).
4. В конце примера нажать на одну из кнопок (рисунок 14). Первая кнопка загрузит пример в текущую вкладку в редакторе кода. Вторая кнопка создает новую вкладку и загрузит код в неё

![](res/example_load_code.jpg)  
**Рис. 14.** Загрузка кода из примера в редактор кода.

#### <a name="title_7"> []()Добавление уроков и файлов pdf формата в программу </a>
-----------------------
[]Программа может отображать новые pdf файлы или новые уроки. Для того, чтобы добавить новый pdf файл для отображения в программе, следует добавить pdf файл в папку «Manuals» (рисунок 15)  

![](res/pdf_files.jpg)  
**Рис. 15.** Добавление файлов pdf формата в программу.

[]Для добавления уроков (необязательно уроков), в папке Tutorials следует создать новую папку. Папка должна содержать файл tutorial.xml (рисунок 16). В этом файле описаны пути к файлам, которые будут отображены в уроке программы (рисунок 17).  

![](res/new_lesson.jpg)  
**Рис. 16.** Создание нового урока.

![](res/xml.jpg)  
**Рис. 17.** Описание файла tutorial.xml.

[]Тег code отвечает за загрузку примеров из урока в редактор кода. В указанном теге файл должен содержать коды всех примеров из урока.  
[]Тег preview загружает картинку для урока.  
[]Тег source непосредственно является содержимым урока. Атрибут category создает новую вкладку для темы уроков. Атрибут displayName устанавливает название для урока. 
Пример создания тестового урока показан на рисунке 18.  

![](res/result_lesson.png)  
**Рис. 18.** Пример нового урока.